﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Standard_Interfaces_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //now let me create some objects of type compare_bottles
            compare_bottles bottle1 = new compare_bottles(1);
            compare_bottles bottle20 = new compare_bottles(2);
            compare_bottles bottle5 = new compare_bottles(3);
            compare_bottles bottle7 = new compare_bottles(4);
            compare_bottles bottle3 = new compare_bottles(5);


            //now let me create some objects of type compare_bottles2 (these do not have the
            //IComparable interface implemented
            compare_bottles2 bottle21 = new compare_bottles2(1);
            compare_bottles2 bottle220 = new compare_bottles2(2);
            compare_bottles2 bottle25 = new compare_bottles2(3);
            compare_bottles2 bottle27 = new compare_bottles2(4);
            compare_bottles2 bottle23 = new compare_bottles2(5);

            //you will notice that I am intentionally giving incorrect
            //names to the bottles. as in not giving them in a sequence. 

            //now I am going to add them to a List based on their variable names

            //creating a list of type compare_bottles
            List<compare_bottles> bottles_list = new List<compare_bottles>();

            //adding all of compare_bottles objects to the list
            bottles_list.Add(bottle1);
            bottles_list.Add(bottle3);
            bottles_list.Add(bottle5);
            bottles_list.Add(bottle7);
            bottles_list.Add(bottle20);

            //creating a list of type compare_bottles2
            List<compare_bottles2> bottles_list2 = new List<compare_bottles2>();

            //adding all of compare_bottles2 objects to the list
            bottles_list2.Add(bottle21);
            bottles_list2.Add(bottle23);
            bottles_list2.Add(bottle25);
            bottles_list2.Add(bottle27);
            bottles_list2.Add(bottle220);

            //now display the lists in its current order
            Console.WriteLine("This is for the one with IComparable implementation");
            foreach (compare_bottles c in bottles_list)
            {
                Console.WriteLine(c.give_bottle_name());
            }

            Console.WriteLine("This is for the one without IComparable implementation");

            foreach (compare_bottles2 c in bottles_list2)
            {
                Console.WriteLine(c.give_bottle_name());
            }
            
            //at this point, the bottle names will be all mixed up

            //doing a sort
            //I want to add here that I have not written a sort operation
            //thanks to the compareto interface implementation, I can use the list's
            //built in sort method
            bottles_list.Sort();

            //I will do a similar sort for comparebottles2 which DOES NOT HAVE IComparable implementation
            //This wont even execute because the sort works on the basis of IComparable
            //since I havent implemented on this class, it wont give a compile error
            //but it will give a run time erro
            //bottles_list2.Sort();

            Console.WriteLine("After doing the sort operation");

            Console.WriteLine("This is for the one with IComparable implementation");
            //displaying the same list, but this time, it will be sorted
            foreach (compare_bottles c in bottles_list)
            {
                Console.WriteLine(c.give_bottle_name());
            }

            //now lets use that enumerable list implementation

            List<compare_bottles2> temporary_collection = new List<compare_bottles2>();

            //create some items of type compare_bottles2
            compare_bottles2 water_testing_1 = new compare_bottles2(10 );
            compare_bottles2 water_testing_2 = new compare_bottles2(15);
            compare_bottles2 water_testing_3 = new compare_bottles2(16);
            compare_bottles2 water_testing_4 = new compare_bottles2(19);

            //add some items to this list
            temporary_collection.Add(water_testing_1);
            temporary_collection.Add(water_testing_2);
            temporary_collection.Add(water_testing_3);
            temporary_collection.Add(water_testing_4);

            //collect the enumerator for this list
            List<compare_bottles2>.Enumerator enumerator = temporary_collection.GetEnumerator();

            Console.WriteLine("Here is the enumerator interface in action");

            //now I can navigate through the list using the enumerator
            //you will notice how this is the same behavior you can recreate by using a for loop
            //only this is much more, well, professional and cool
            while(enumerator.MoveNext()) //this means, there is another item in the list
            {
                //now I can use the current object being reference by the enumerator 
                //and call the methods on it
                //here I am going to simply display the bottle
                Console.WriteLine(enumerator.Current.give_bottle_name());
            }




            //this is to stop console display from vanishing
            Console.ReadLine();
        }
    }


    //now implementing the IComparable interface
    class compare_bottles : IComparable
    {
        //bottle serial number
        //the serial number is the reference for sorting the bottles
        public int bottle_serial_no;

        //constructor that assigns a value to the bottle
        public compare_bottles(int temp_creation)
        {
            bottle_serial_no = temp_creation;
        }

        public string give_bottle_name()
        {
            //return this string
            return "the bottle name is " + bottle_serial_no;
        }

        public int CompareTo(object obj)
        {
            //check for null objects
            if (obj == null) return 1;

            compare_bottles wb = obj as compare_bottles;

            //make sure that the the object sent is of the correct type
            if (wb == null)
                throw new ArgumentException();

            //now I will compare the serial no of the current object
            //with that of the serial no of the object in the parameter
            return this.bottle_serial_no.CompareTo(wb.bottle_serial_no);
        }
    }

    //just to demonstrate the benefit of using the interface IComparable
    //I will create another class but without IComparable

    class compare_bottles2
    {
        //bottle serial number
        //the serial number is the reference for sorting the bottles
        public int bottle_serial_no;

        //constructor that assigns a value to the bottle
        public compare_bottles2(int temp_creation)
        {
            bottle_serial_no = temp_creation;
        }

        public string give_bottle_name()
        {
            //return this string
            return "the bottle name is " + bottle_serial_no;
        }
    }

    //now I am going to implement the IEnumerable interface
    //I am going to use the class compare_bottles2 on this

    class water_bottle_collection : IEnumerable<compare_bottles2>
    {
        //assigning the list that is sent while creating the object
        //to the local collection
        public water_bottle_collection(compare_bottles2[] temp_water_bottle_collection)
        {
            this.temp_water_bottle_collection = temp_water_bottle_collection;
        }

        //creating a list of type compare_bottles2
        compare_bottles2[] temp_water_bottle_collection;

        //this returns that enumerable list
        public IEnumerator<compare_bottles2> GetEnumerator()
        {
            for(int i =0;i < temp_water_bottle_collection.Length;i++)
            {
                //the yeild is the special keyword that only with iterators such as this
                //what yeild does is convert the following statement into a state machine.
                //check our blog for more info about state machines
                yield return temp_water_bottle_collection[i];
            }
        }

        //this returns the enumerator.
        //this is kind of like the index for your list
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
